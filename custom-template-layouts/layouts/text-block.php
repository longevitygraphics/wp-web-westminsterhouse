<?php

	include get_template_directory() . '/custom-template-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="d-flex <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
	<div class="col-12">
		<?php echo $text; ?>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/custom-template-layouts/partials/block-settings-end.php';

?>