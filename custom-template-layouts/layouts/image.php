<section class="custom-layout-section ">
	<div class="image-block py-5 px-3">
		<?php  
			$image = get_sub_field('image');
			$size = 'full'; // (thumbnail, medium, large, full or custom size)
			if( $image ) {
			    echo wp_get_attachment_image( $image, $size );
			}
		?>
	</div>
</section>