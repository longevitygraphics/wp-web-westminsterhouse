<section class="custom-layout-section">
	<div class="py-5 px-3 two-column-layout d-flex">
		<?php  
			$image = get_sub_field('image');
			$text = get_sub_field('text');
			$size = 'full';
		?>
		<div class="two-column-image col-12 col-md-6">
			<?php 
				if( $image ) {
				    echo wp_get_attachment_image( $image, $size );
				    echo 'test';
				} 
			?>
		</div>
		<div class="two-column-text col-12 col-md-6">
			<?php 
				if( $text ) {
				    echo $text;
				} 
			?>
		</div>
	</div>
</section>