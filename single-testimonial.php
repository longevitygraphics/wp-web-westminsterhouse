<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h2 class="title-lead"><a href="<?php echo esc_url( home_url('/about/') ); ?>">About</a></h2>
			<h6>Testimonials</h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
			<div class="entry-thumbnail">
				<?php if( has_post_thumbnail() ): the_post_thumbnail('thumbnail'); 
					else: echo '<img src="'. get_stylesheet_directory_uri() .'/images/staff-thumbnail.png" alt="" />'; 
				endif; ?>
			</div>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<h5><?php $title = get_field('staff_title'); echo strtolower($title); ?></h5>
				<ul class="contact-info-list">
					<?php if( get_field('staff_phone') ): ?>
					<li><a href="tel:<?php the_field('staff_phone'); ?>"><i class="fa fa-phone"></i></a></li>
					<?php endif; ?>
					<?php if( get_field('staff_email') ): ?>
					<li><a href="mailto:<?php the_field('staff_email'); ?>"><i class="fa fa-envelope"></i></a></li>
					<?php endif; ?>
					<?php if( get_field('staff_website') ): ?>
					<li><a href="<?php the_field('staff_website'); ?>"><i class="fa fa-link"></i></a></li>
					<?php endif; ?>
					<?php if( get_field('staff_linkedin') ): ?>
					<li><a href="<?php the_field('staff_linkedin'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					<?php endif; ?>
					<?php if( get_field('staff_twitter') ): ?>
					<li><a href="mailto:<?php the_field('staff_twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<?php endif; ?>
				</ul>
			</header>

			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<footer class="entry-footer">
				<?php westminster_entry_footer(); ?>
			</footer>
		</article>

		<?php the_post_navigation(); 
		endwhile; ?>

		</main>
	</div>

<?php get_footer(); ?>
