<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<!-- <?php global $post; 
				$blog_page 	= get_option('page_for_posts');
				$blog_title	= get_the_title( $blog_page ); 
				$blog_url 	= get_permalink( $blog_page ); 
			?>
			<h2 class="title-lead"><a href="<?php echo $blog_url; ?>"><?php echo $blog_title; ?></a></h2> -->
			<h2 class="title-lead"><a href="/staff">About</a></h2>
			<h6>Staff & Affiliates</h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="staff-search-bar">
				<span class="all-staff"><a href="<?php echo esc_url(home_url('/staff/')); ?>">All Staff</a></span>
				<?php westminster_staff_dept_search(); ?>
			</div>

			<div id="archives" class="flex-container">
			<?php while ( have_posts() ): the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('archive'); ?>>
					<header class="entry-header">
						<div class="entry-thumbnail">
							<a href="<?php the_permalink(); ?>">
								<?php if( has_post_thumbnail() ): the_post_thumbnail('medium'); 
									else: echo '<img src="'. get_stylesheet_directory_uri() .'/images/staff-thumbnail.png" alt="" />'; 
								endif; ?></a>
						</div>
						<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<h5 class="credentials">&nbsp;<?php $credentials = get_field('staff_credentials'); echo strtolower($credentials); ?></h5>
						<h5><?php $title = get_field('staff_title'); echo strtolower($title); ?></h5>
					</header>

					<div class="staff-contact">
						<ul class="contact-info-list">
							<?php if( get_field('staff_email') ): ?>
							<li><a href="mailto:<?php the_field('staff_email'); ?>"><i class="fa fa-envelope"></i></a></li>
							<?php endif; ?>
							<?php if( get_field('staff_website') ): ?>
							<li><a href="<?php the_field('staff_website'); ?>" target="_blank"><i class="fa fa-link"></i></a></li>
							<?php endif; ?>
							<?php if( get_field('staff_linkedin') ): ?>
							<li><a href="<?php the_field('staff_linkedin'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<?php endif; ?>
							<?php if( get_field('staff_twitter') ): ?>
							<li><a href="mailto:<?php the_field('staff_twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<?php endif; ?>
						</ul>
					</div>
				</article>

			<?php endwhile; ?>
			</div>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
