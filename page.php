<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		
		<div class="overlay"></div>
		<div class="page-title">
			<?php if( is_page('education') ): echo '<h2 class="title-lead"><a href="'. esc_url( home_url('/initiatives/') ) .'">Initiatives</a></h2>';
			elseif( $post->post_parent ): 
				$parent_link = get_permalink($post->post_parent); ?>
			<h2 class="title-lead"><a href="<?php echo $parent_link ?>"><?php echo get_the_title($post->post_parent); ?></a></h2>
			<?php endif; ?>
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if( is_page( array('donate') ) ): ?>
			<div id="donate-today">
				<h1>Join us in our shared belief that treatment works</h1>
				<a class="btn white" href="https://www.canadahelps.org/dn/15610" target="_blank">Donate Today</a>
			</div>
			<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<?php include get_stylesheet_directory() . '/inc/page-landing-top.php'; ?>
				<?php get_template_part( 'template-parts/content', 'page' );  ?>
				<?php include get_stylesheet_directory() . '/inc/page-landing-bottom.php'; ?>
			<?php endwhile; ?>

			<?php if( is_page('mothers-day') && function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 5 ); } ?>

			<?php if ( get_field('document_pdf') ): ?>
			<div class="documents">
				<h1>Downloads</h1>
				<div class="documents-content">
				<?php $attachment_id = get_field('document_pdf');
					$url = wp_get_attachment_url( $attachment_id );
					$title = get_the_title( $attachment_id );
					$path_info = pathinfo( get_attached_file( $attachment_id ) );
				// part where to get the filesize
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);
				?>
				<?php if (get_the_ID() == '41'): ?>
					<div> For Self Referrers </div><br />
				<?php endif ?>
				<a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a> <br />
				<span class="file-specs"><i class="fa fa-file-pdf-o"></i> <?php echo $path_info['extension'] ?>, <?php echo $filesize; ?></span>
				<?php if ( get_field('secondary_pdf') ): ?>
					<?php
						$secondary_attachement_id = get_field('secondary_pdf');
						$secondary_url = wp_get_attachment_url( $secondary_attachement_id );
						$secondary_title = get_the_title( $secondary_attachement_id );
						$secondary_path_info = pathinfo( get_attached_file( $secondary_attachement_id ) );
						$secondary_filesize =filesize( get_attached_file( $secondary_attachement_id ) );
						$secondary_filesize = size_format($secondary_filesize, 2);
					?>
					<br />

					<div class="documents-subtitle"> For Referring Agents </div> <br /> <a href="<?php echo $secondary_url; ?>" target="_blank"><?php echo $secondary_title; ?></a> <br />
					<span class="file-specs"><i class="fa fa-file-pdf-o"></i> <?php echo $secondary_path_info['extension'] ?>, <?php echo $secondary_filesize; ?></span>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>

		</main>
	</div>

<?php get_footer(); ?>