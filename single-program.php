<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h2 class="title-lead"><a href="<?php echo esc_url( home_url('/treatment-programs/') ); ?>">Treatment Programs</a></h2> 
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ): the_post(); ?>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header>

			<div class="entry-thumbnail">
				
			</div>

			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<?php if ( get_field('document_pdf') ): ?>
			<div class="documents">
				<h1>Downloads</h1>
				<?php $attachment_id = get_field('document_pdf');
					$url = wp_get_attachment_url( $attachment_id );
					$title = get_the_title( $attachment_id );
					$path_info = pathinfo( get_attached_file( $attachment_id ) );
				// part where to get the filesize
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);
				?>
				<p><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a> <br />
				<span class="file-specs"><i class="fa fa-file-pdf-o"></i> <?php echo $path_info['extension'] ?>, <?php echo $filesize; ?></span></p>
			</div>
			<?php endif; ?>

			<footer class="entry-footer">
				<?php westminster_entry_footer(); ?>
			</footer>
		</article>

		<?php the_post_navigation(); 
		endwhile; ?>

		</main>
	</div>

<?php get_footer(); ?>
