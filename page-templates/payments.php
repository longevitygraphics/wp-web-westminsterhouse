<?php
/**
 * Template Name: Payments Page
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if( is_page('approved') ): ?>
			<div id="payment-confirmation" class="clear">
				<h1><img class="transaction-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/approved.svg" alt="" /> Thank you for your <span class="transaction-type"><?php echo htmlspecialchars($_GET["ref1"]); ?></span> payment!</h1>
			</div>
			<p>Your payment of <span class="transaction-amount">&dollar;<?php echo htmlspecialchars($_GET["trnAmount"]); ?></span> has been confirmed.</p>
			<?php endif; ?>

			<?php if( is_page('declined') ): ?>
			<div id="payment-confirmation" class="clear">
				<h1><img class="transaction-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/declined.svg" alt="" /> Sorry, we could not complete your transaction</h1>
			</div>
			<p>Your payment of <span class="transaction-amount">&dollar;<?php echo htmlspecialchars($_GET["trnAmount"]); ?></span> has been declined.</p>
			<?php endif; ?>

			<?php while ( have_posts() ) : the_post();
	  			if( is_page( array('approved', 'declined') )): 
					the_content(); 
				else:
	  				the_title( '<h2 class="entry-title">', '</h2>' ); 
	  				the_content(); 
	  		endif; endwhile; ?>

	  		<?php if( is_page('payments') ): ?>
			<form method="POST" id="paymentform" class="payment-form" action="https://www.beanstream.com/scripts/payment/payment.asp?merchant_id=225810897&trnLanguage=eng&hashValue=5757f8b88f5ed98b2db79ddc56477d9752275b1d&passcode=BDC18C95172946DF9296189ABB3D6E92">
				<fieldset>
					<legend>Step 1<br />
					<span class="required"><sup>*</sup> All Fields Required</span></legend>
					<input type="hidden" name="merchant_id" value="225810897" title="Merchant ID">
					<div class="payment-application">
						<label>Payment For<span class="required"><sup>*</sup></span></label>
						<select id="reference" name="ref1" required>
							<option value="" selected disabled>-- Please Select --</option>
							<option name="treatment-invoice" value="Treatment Invoice">Treatment Invoice</option>
							<option name="medication-invoice" value="Medication Invoice">Medication Invoice</option>
							<option name="allowance-invoice" value="Allowance Invoice">Allowance Invoice</option>
							<option name="merchandise-receipt" value="Merchandise Receipt">Merchandise Receipt</option>
							<option name="donation" value="Donation">Donation</option>
							<option name="raffle-ticket" value="Raffle Ticket">Raffle Ticket</option>
						</select>
					</div>
					<div class="payment-number">
						<label for="trnOrderNumber">Reference #<span class="required"><sup>*</sup></span></label>
						<input id="order-number" type="text" name="trnOrderNumber" required>
						<div id="helper-text">Please enter the Raffle Ticket number(s) above.</div>
					</div>
					<div class="payment-amount">
						<label for="trnAmount">Amount<span class="required"><sup>*</sup></span> <span class="currency">CAD$</span></label>
						<input type="text" name="trnAmount" required>
					</div>
					<div class="payment-button clear">
						<button type="submit" class="payment-submit" value="Submit Payment">Continue <i class="fa fa-angle-right"></i></button>
						<span class="secure">
							<a style="text-decoration: none" href="https://www.beanstream.com/scripts/valid_merchant.asp?merchantId=225810897" target="_new"><img src="https://www.beanstream.com/public/assets/images/media/beanstream_secure/beanstream_secure_light.gif" border="0" width="57" height="30" alt="Secure Electronic Payment Processing"></a>
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/visa.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mastercard.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/discover.svg" alt="" />
						</span>
					</div>
				</fieldset>
			</form>
<script>
jQuery(function() {
    jQuery('#helper-text').hide(); 
    jQuery('#reference').change(function(){
        if(jQuery('#reference').val() == 'Raffle Ticket') {
            jQuery('#helper-text').show(); 
        } else {
            jQuery('#helper-text').hide(); 
        } 
    });
    jQuery('#reference').change(function(){
        if(jQuery('#reference').val() == 'Donation') {
            jQuery('#order-number').val(jQuery(this).val());
        } else {
            jQuery('#order-number').val(''); 
        } 
    });
});
</script>
			<?php endif; ?>
		</main>
	</div>

<?php get_footer(); ?>
