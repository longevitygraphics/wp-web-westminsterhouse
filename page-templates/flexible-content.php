<?php
/**
 * Template Name: Flexible Content
 * Template Post Type: post, page, program
 * @package Westminster
 */

get_header(); ?>

	<div id="primary" class="content-area flexbile-content-template">
		<main id="main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php include get_stylesheet_directory() . '/acf-layouts/layouts.php'; ?>
			<?php endwhile; ?>
			
		</main>
	</div>

<?php get_footer(); ?>