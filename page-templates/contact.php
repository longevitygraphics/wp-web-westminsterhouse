<?php
/**
 * Template Name: Contact Page
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); 
				get_template_part( 'template-parts/content', 'page' ); 
			endwhile; ?>

			<?php 
				$phone1 	= get_field('contact_phone1', 'option');
				$phone2 	= get_field('contact_phone2', 'option');
				$email 		= get_field('contact_email', 'option');
				$location 	= get_field('contact_address', 'option');
			if( ! empty($location) ): ?>
			<div class="map-wrapper">
				<div id="location_map">
					<div class="google-map"></div>
					<script>
					//<![CDATA[
					function initMap() {
						var lat = <?php echo $location['lat']; ?>;
						var lng = <?php echo $location['lng']; ?>;
					// coordinates to latLng
						var latlng = new google.maps.LatLng(lat, lng);
					// map Options
						var myOptions = {
							zoom: 14,
							center: latlng,
							scrollwheel: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP,
						};
					// map Styles
						var stylesArray = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}];
					//draw a map
						var map = new google.maps.Map(document.getElementById("location_map"), myOptions);
						var contentString = '<div id="marker-content">'+
							'<img class="map-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/westminster-house.svg" alt="<?php echo get_bloginfo( 'name' ); ?> <?php echo get_bloginfo( 'description' ); ?>" />'+
							'<p><a href="tel:+1-<?php echo $phone1 ?>"><?php echo $phone1 ?></a></p>'+
							'<p><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></p>'+
							'<a class="btn small more" href="https://www.google.ca/maps/dir/Current+Location/<?php echo $location['address']; ?>" target="_blank"><i class="fa fa-map-marker"></i> Directions</a>'+
							'</div>';
						var image = '<?php echo get_stylesheet_directory_uri(); ?>/images/map-marker.png';
						var infowindow = new google.maps.InfoWindow({
							content: contentString,
							maxWidth: 500
						});
						var marker = new google.maps.Marker({
							position: map.getCenter(),
							map: map,
							icon: image
						});
							map.setOptions({styles: stylesArray});
							marker.addListener('click', function() {
							infowindow.open(map, marker);
						});
					}
					//]]>
					</script>
					<script src='http://maps.googleapis.com/maps/api/js?key=AIzaSyDMP_EAB7zmhPNrWxc6_W31SM-FB1l_VRI&callback=initMap' async defer></script>
				</div>
			</div>
			<?php endif; ?> 

		</main>
	</div>

<?php get_footer(); ?>
