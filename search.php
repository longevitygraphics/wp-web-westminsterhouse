<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h6>Search</h6>
		</div>
	</div>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'westminster' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header>

			<?php while ( have_posts() ) : the_post(); 
				get_template_part( 'template-parts/content', 'search' ); 
			endwhile; ?>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</section>

<?php get_footer(); ?>
