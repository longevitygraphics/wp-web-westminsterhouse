<?php 
/**
 * Text Block Layout
 *
 */
?>
<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="d-flex image-gallery <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
	<div class="col-12">
		<div class="row">
			<?php
			if( have_rows('images') ):
			    while ( have_rows('images') ) : the_row();
			        $image = get_sub_field('image');
			        $space = get_sub_field('space');
			        ?>
					<div class="col-md-<?php echo $space; ?>"><img class="img-full" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
			        <?php
			    endwhile;
			else :
			    // no rows found
			endif;
			?>
		</div>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>
