<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="d-flex flexible_call-to-action <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row">
	<?php
		$copy = get_sub_field('copy');
		$button = get_sub_field('button');
		$link_target = $button['target'] ? $button['target'] : '_self';
	?>
	<div class="col-12 d-flex flex-wrap flex-column flex-md-row justify-content-between align-items-start align-items-md-center">
		<div class="copy mb-3 mb-md-0">
			<?php echo $copy; ?>
		</div>
		<div class="button">
			<?php if($button): ?>
				<a target="<?php echo esc_attr($link_target); ?>" class="btn-primary btn-lg" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>
