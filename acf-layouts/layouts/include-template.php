<?php 
/**
 * Media Block Layout
 *
 */
?>
<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php $template_to_load = get_sub_field('template_to_load'); ?>

<div class="d-flex <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
	<div class="col-12">
		<?php get_template_part($template_to_load); ?>
	</div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>