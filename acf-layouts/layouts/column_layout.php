<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php  if(have_rows("column_content")) : ?>
	<div class="column_layout row">
		<?php while(have_rows("column_content")) : the_row(); ?>
			<div class="column-content col-md-4">
				<?php 
					$column_image = get_sub_field("column_image");
					$column_text = get_sub_field("column_text");
				?>
				<img src="<?php echo $column_image['url']; ?>" alt="<?php echo $column_image['alt']; ?>" />
				<p><?php echo $column_text; ?></p>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>