<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$grid_settings = get_sub_field('grid_settings');
	$grid_responsive = get_sub_field('grid_responsive');
	$no_gutters = $grid_settings['no_gutter'];

	$item_per_row_large = (int)$grid_responsive['item_per_row_large'];
	$item_per_row_medium = (int)$grid_responsive['item_per_row_medium'];
	$item_per_row_small = (int)$grid_responsive['item_per_row_small'];
	$item_per_row_extra_small = (int)$grid_responsive['item_per_row_extra_small'];

?>

<div class="d-flex grid-list-hover-overlay <?php if($container == 'container-wide' || $no_gutters == 1){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	<?php if($items && is_array($items)): ?>
		<?php foreach ($items as $key => $value): ?>
			<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-<?php echo $item_per_row_extra_small; ?> col-sm-<?php echo $item_per_row_small; ?> col-md-<?php echo $item_per_row_medium; ?> col-lg-<?php echo $item_per_row_large; ?> d-block">

				<div class="image">
					<img src="<?php echo $value['image']['url']; ?>" alt="<?php echo $value['image']['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $value['description']; ?></div>
						<?php $link_target = $value['cta']['target'] ? $value['cta']['target'] : '_self'; ?>
						<a target="<?php echo esc_attr($link_target); ?>" href="<?php echo $value['cta']['url']; ?>" class="btn-transparent"><?php echo $value['cta']['title']; ?></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $value['title']; ?></div>
				</div>

			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>