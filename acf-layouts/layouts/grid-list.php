<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	include get_template_directory() . '/acf-layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$grid_settings = get_sub_field('grid_settings');
	$grid_responsive = get_sub_field('grid_responsive');
	$no_gutters = $grid_settings['no_gutter'];

	$item_per_row_large = (int)$grid_responsive['item_per_row_large'];
	$item_per_row_medium = (int)$grid_responsive['item_per_row_medium'];
	$item_per_row_small = (int)$grid_responsive['item_per_row_small'];
	$item_per_row_extra_small = (int)$grid_responsive['item_per_row_extra_small'];

?>

<div class="d-flex <?php if($container == 'container-wide' || $no_gutters == 1){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>" <?php if($no_gutters != 1): ?>style="margin-top: -7.5px;margin-bottom: -7.5px;"<?php endif; ?>>
	<?php if($items && is_array($items)): ?>
		<?php foreach ($items as $key => $value): ?>
			<?php $mobile_order = $value['mobile_order']; ?>
			<div <?php if($no_gutters != 1): ?>style="padding-top: 7.5px; padding-bottom: 7.5px;"<?php endif; ?> class="col-<?php echo $item_per_row_extra_small; ?> col-sm-<?php echo $item_per_row_small; ?> col-md-<?php echo $item_per_row_medium; ?> col-lg-<?php echo $item_per_row_large; ?> d-block <?php if($mobile_order): ?>order-md-initial order-<?php echo $mobile_order; ?><?php endif; ?>">

			<?php if($value['content_type'] == 'text'): ?>
				<?php
					$text = $value['content_text'];
					include(locate_template('/acf-layouts/components/text.php')); 
				?>
			<?php elseif($value['content_type'] == 'image'): ?>
				<?php
					$image = $value['content_image']['image'];
					include(locate_template('/acf-layouts/components/image.php')); 
				?>
			<?php elseif($value['content_type'] == 'gallery'): ?>
				<?php
					$gallery = $value['content_gallery']['gallery'];
					include(locate_template('/acf-layouts/components/gallery.php')); 
				?>
			<?php elseif($value['content_type'] == 'video'): ?>
				<?php
					$video = $value['content_video']['video'];
					include(locate_template('/acf-layouts/components/video.php')); 
				?>
			<?php endif; ?>

			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/acf-layouts/partials/block-settings-end.php';

?>