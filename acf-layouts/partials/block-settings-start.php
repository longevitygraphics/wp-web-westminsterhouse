<?php 
/**
 * General Block Partial Start
 *
 */
	
	$block_classes = '';
	$container_classes = '';
	$css_id = get_sub_field('css_id');

	// Background Colours
	$background = get_sub_field('background'); 
	$background_type = $background['type'];

	$background_color = $background['colour'];
	$background_image = $background['image'];

	$container = get_sub_field('container');

	if($background_type == 'Colour' && $background_color){
		$block_classes .= 'bg-' . $background_color . ' ';
	}

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');

	foreach ($block_padding as $key => $value) {
		if($value){
			$container_classes .= $value . ' ';
		}
	}


	$block_margin  = get_sub_field('block_margin'); 

	foreach ($block_margin as $key => $value) {
		if($value){
			$block_classes .= $value . ' ';
		}
	}

	$block_title = get_sub_field('block_title');

?> 

<section <?php if($css_id): ?>id="<?php echo $css_id; ?>"<?php endif; ?> class="flexible-content <?php the_sub_field('custom-classes'); ?> <?php echo $block_classes; ?>" <?php if($background_type == 'Image' && $background_image){ echo 'style="background-image: url(\'' . $background_image . '\')"'; } ?> >
	<div class="<?php echo $container; ?>  <?php echo $container_classes; ?>">
		<?php if(get_sub_field('block_title_show') == 1): ?>
			<h2 class="block-title mb-4"><?php echo $block_title; ?></h2>
		<?php endif; ?>
		