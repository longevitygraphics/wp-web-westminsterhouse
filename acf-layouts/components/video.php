<?php

	// $video

	$src_from = $video['src_from'];
	$src_file = $video['src_file'];
	$src_embed = $video['src_embed'];
	$settings = $video['settings'];
	$size = $settings['size'];
	$width = $settings['video_width'];
	$aspect_ratio = $settings['video_aspect_ratio'];
	$poster = $settings['poster'];
	$autoplay = $settings['autoplay'];
	$loop = $settings['loop'];
	$controls = $settings['controls'];

?>

	
	<?php if($src_from == 'file'): ?>
		<video <?php if($controls == 1): ?>controls <?php endif; ?> <?php if($autoplay == 1): ?>autoplay <?php endif; ?> <?php if($loop == 1): ?>loop <?php endif; ?> <?php if($poster): ?>poster="<?php echo $poster; ?>"<?php endif; ?> style="<?php if($size == 'full-width'): ?> width: 100%; <?php else: ?> width: 100%; max-width: <?php echo $width; ?> <?php endif; ?>" src="<?php echo $src_file; ?>"></video>
	<?php elseif($src_from == 'embed'): ?>
		<div style="<?php if($size == 'full-width'): ?> width: 100%; <?php else: ?> width: 100%; max-width: <?php echo $width; ?> <?php endif; ?>" class="embed-responsive <?php echo $aspect_ratio; ?>">
			<?php echo $src_embed; ?>
		</div>
	<?php endif; ?>