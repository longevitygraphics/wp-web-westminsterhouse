<?php

	// $image
	$src = $gallery['src'];
	$settings = $gallery['settings'];
	$fade = $settings['fade'];
	$infinite = $settings['infinite'];
	$autoplay = $settings['autoplay'];
	$arrows = $settings['arrows'];
	$dots = $settings['dots'];
	$autoplay_speed = $settings['autoplay_speed'];

?>

	<?php if($src && is_array($src)): ?>
		<div class="lg-gallery">
			<?php foreach ($src as $key => $value): ?>
				<div>
					<img class="img-full" src="<?php echo $value['image']['url']; ?>" alt="<?php echo $value['image']['alt']; ?>">
					<div class="caption">
						<?php echo $value['caption']; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<script>
		(function($) {

		    $(document).ready(function(){
		    	$('.lg-gallery').slick({
					arrows: <?php echo ($arrows == 1 ? 'true' : 'false'); ?>,
					dots: <?php echo ($dots == 1 ? 'true' : 'false'); ?>,
					fade: <?php echo ($fade == 1 ? 'true' : 'false'); ?>,
					autoplay: <?php echo ($autoplay == 1 ? 'true' : 'false'); ?>,
					autoplaySpeed: <?php echo ($autoplay_speed ? $autoplay_speed * 1000 : 5000 ); ?>,
		    		variableWidth: false,
		    		adaptiveHeight: true
				});
		    });

		}(jQuery));
	</script>
	