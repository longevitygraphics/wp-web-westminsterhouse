<?php if( have_rows('flexible_content_block') ):
	while ( have_rows('flexible_content_block') ) : the_row();
		switch ( get_row_layout()) {
			case 'text_block':
				get_template_part('/acf-layouts/layouts/text-block');
			break;

			case 'grid_-_list':
				get_template_part('/acf-layouts/layouts/grid-list');
			break;

			case 'full_width_media':
				get_template_part('/acf-layouts/layouts/media');
			break;

			case 'grid_with_hover_overlay':
				get_template_part('/acf-layouts/layouts/grid-list-hover-overlay');
			break;

			case 'call_to_action':
				get_template_part('/acf-layouts/layouts/call-to-action');
			break;

			case 'image_gallery':
				get_template_part('/acf-layouts/layouts/image-gallery');
			break;

			case 'include_template':
				get_template_part('/acf-layouts/layouts/include-template');
			break;

			case 'column_layout':
				get_template_part('/acf-layouts/layouts/column_layout');
			break;

		}

	endwhile;
	else : // no layouts found 
endif; ?>