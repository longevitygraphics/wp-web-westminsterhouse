<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<style>
		:root{
			--color-blue: #2e5ead;
		}
		h6{
			font-family: 'jbt-annabelle', Helvetica, sans-serif;
		}
		.title{
			color: var(--color-blue);
		}
		#primary{
			padding: 0;
		}
		.img-full{
			width: 100%;
			display: block;
		}
		</style>

		<div id="page-thumb">
			<div class="overlay"></div>
			<div class="page-title">
				<?php if( is_page('education') ): echo '<h2 class="title-lead"><a href="'. esc_url( home_url('/initiatives/') ) .'">Initiatives</a></h2>';
				elseif( $post->post_parent ): 
					$parent_link = get_permalink($post->post_parent); ?>
				<h2 class="title-lead"><a href="<?php echo $parent_link ?>"><?php echo get_the_title($post->post_parent); ?></a></h2>
				<?php endif; ?>
				<h6><?php the_title(); ?></h6>
			</div>
		</div>

		<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ): 
				while ( have_posts() ): the_post(); ?>
			<div class="entry-content container">
				<?php
					$title = get_field('title');
					$section_one = get_field('section_one');
					$section_two = get_field('section_two');
					$section_three = get_field('section_three');
					$form = get_field('form');
				?>
				<h1 class="text-center title mb-5 mt-4"><?php echo $title; ?></h1>

				<div class="row mb-5">
					<div class="col-12">
						<?php echo $section_one['text']; ?>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-sm-4">
						<img class="img-full" src="<?php echo $section_two['image']['url']; ?>" alt="<?php echo $section_two['image']['alt']; ?>">
					</div>
					<div class="col-sm-8">
						<?php echo $section_two['text']; ?>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-sm-8">
						<?php echo $section_three['text']; ?>
					</div>
					<duv class="col-sm-4">
						<img class="img-full" src="<?php echo $section_three['image']['url']; ?>" alt="<?php echo $section_three['image']['alt']; ?>">
					</duv>
				</div>

			<form method="POST" id="paymentform" class="payment-form" action="https://www.beanstream.com/scripts/payment/payment.asp?merchant_id=225810897&trnLanguage=eng&hashValue=5757f8b88f5ed98b2db79ddc56477d9752275b1d&passcode=BDC18C95172946DF9296189ABB3D6E92">
				<fieldset>
					<div class="px-3">
						<table class="table">
							<tr>
								<th>DESCRIPTION</th>
								<th>AMOUNT</th>
								<th>NUMBER</th>
								<th>TOTAL</th>
							</tr>
							<tr>
								<td>1 Card with Envelope</td>
								<td>$5.00</td>
								<td><input type="number" single="5" min="0" class="buy form-control"></td>
								<td><div class="total"></div></td>
							</tr>
							<tr>
								<td>5 Card with Envelope</td>
								<td>$20.00</td>
								<td><input type="number" single="20" min="0" class="buy form-control"></td>
								<td><div class="total"></div></td>
							</tr>
							<tr>
								<td>10 Card with Envelope</td>
								<td>$35.00</td>
								<td><input type="number" single="35" min="0" class="buy form-control"></td>
								<td><div class="total"></div></td>
							</tr>
							<tr>
								<td>Christmas ball ornament</td>
								<td>$10.00</td>
								<td><input type="number" single="5" min="0" class="buy form-control"></td>
								<td><div class="total"></div></td>
							</tr>
							<tr>
								<td>Overall</td>
								<td></td>
								<td></td>
								<td><input class="d-none" type="text" name="trnAmount"><div class="overall"></div></td>
							</tr>
						</table>
					</div>
					<input type="hidden" name="merchant_id" value="225810897" title="Merchant ID">
					<div class="payment-application d-none">
						<input type="text" id="reference" name="ref1" value="Donation">
					</div>
					<div class="payment-number d-none">
						<input id="order-number" type="text" name="trnOrderNumber" value="Christmas Cards">
					</div>
					<div class="payment-button clear">
						<button type="submit" class="payment-submit" value="Submit Payment">Continue <i class="fa fa-angle-right"></i></button>
						<span class="secure">
							<a style="text-decoration: none" href="https://www.beanstream.com/scripts/valid_merchant.asp?merchantId=225810897" target="_new"><img src="https://www.beanstream.com/public/assets/images/media/beanstream_secure/beanstream_secure_light.gif" border="0" width="57" height="30" alt="Secure Electronic Payment Processing"></a>
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/visa.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mastercard.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/discover.svg" alt="" />
						</span>
					</div>
				</fieldset>
			</form>
			</div>
			<?php endwhile; endif; ?>
		</main>
	</div>

	<script>
	(function($) {

	    $(document).ready(function(){

	    	$('.buy').on('change', function(){
	    		var amount = parseFloat($(this).val()) * parseFloat($(this).attr('single'));
	    		var total = $(this).parent().next().find('.total');
	    		total.val(amount.toFixed(2)).html('$' + amount.toFixed(2));

	    		var overall = updateTotal();
	    		$('input[name=trnAmount]').val(overall).siblings('.overall').html('$' + overall.toFixed(2));

	    	});

	    	function updateTotal(){
	    		var total = 0;

	    		$('.total').each(function(){
	    			console.log($(this).text());
	    			var single = $(this).text() ? parseFloat($(this).text().substring(1)) : 0;
	    			total += single;
	    		});

	    		return total;
	    	}
	        
	    });

	}(jQuery));
	</script>

<?php get_footer(); ?>
