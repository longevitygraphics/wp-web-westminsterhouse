<?php
/**
 * @package Westminster
 */

get_header(); ?>
    
	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<!-- <?php global $post; 
				$blog_page 	= get_option('page_for_posts');
				$blog_title	= get_the_title( $blog_page ); 
				$blog_url 	= get_permalink( $blog_page ); 
			?>
			<h2 class="title-lead"><a href="<?php echo $blog_url; ?>"><?php echo $blog_title; ?></a></h2> -->
			<h2 class="title-lead"><a href="/how-to-help">How to Help</a></h2>
			<h6>Faqs</h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<p>Below is a list of Frequently Asked Questions covering some of the most common questions we respond to.</p>
			<p>If you have further queries, please <a href="<?php echo esc_url( home_url( '/contact/' ) ); ?>">contact us</a> and we would be happy to answer any questions you have.</p>

			<div id="faqs" class="page-content">

			<?php while ( have_posts() ) : the_post(); ?>

				<h3 class="faq-question"><a href="#"><?php the_title(); ?></a></h3>
				<div class="faq-answer"><?php the_content(); ?></div>

			<?php endwhile; ?>

			</div>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
