<?php
/**
 * @package Westminster
 */

?>

	</div>

	<span class="back-to-top"><a href="#"><i class="fa-fw fa fa-angle-up"></i></a></span>

	<?php if ( is_front_page() ): ?>
	<div id="footer-newsletter">
		<h2>Stay up to date with our Newsletter!</h2>
		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup">
			<form action="http://westminsterhouse.us2.list-manage1.com/subscribe/post?u=f3b4cf1e054ebbb7b35cfb970&id=73f5f45bd7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
				<div class="clear">
					<button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn black">Subscribe</button>
				</div>
			</form>
		</div>
		<!--End mc_embed_signup-->
	</div>
	<?php endif; ?>

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div id="cta-donate">
			<h2 class="h1 text-white">Join us in our shared belief that treatment works</h2>
			<a class="btn white" href="https://www.canadahelps.org/dn/15610" target="_blank">Donate Today</a>
		</div>

		<div id="footer-widget-area" class="flex-container">

			<div class="footer-menu widget">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' ) ); ?>
			</div>

			<div class="footer-contact widget">
				<?php 
					$phone1	= get_field('contact_phone1', 'option'); 
					$phone2	= get_field('contact_phone2', 'option'); 
					$email 	= get_field('contact_email', 'option'); 
					$address = get_field('contact_address', 'option');					
				?>
				<ul class="fa-ul">
					<li><i class="fa-li fa fa-map-marker"></i> 
						<?php if( get_field('address_text', 'option') ): 
							the_field('address_text', 'option'); ?>
						<?php endif; ?>
						 <?php echo $address['address']; ?> | <a href="https://www.google.com/maps/place/<?php echo $address['address']; ?>/@<?php echo $address['lat'] . ',' . $address['lng']; ?>,16z" target="_blank"><b>View Map</b></a></li>
					<li><?php if( $phone1 ): ?><i class="fa-li fa fa-phone"></i> <a href="tel:<?php echo $phone1 ?>"><b><?php echo $phone1 ?></b></a><?php endif; ?> | <?php if( $phone2 ): ?><a href="tel:<?php echo $phone2 ?>"><b><?php echo $phone2 ?></b></a><?php endif; ?></li>
					<?php if( $email ): ?>
					<li><i class="fa-li fa fa-envelope"></i> <a href="mailto:<?php echo $email ?>"><b><?php echo $email ?></b></a></li>
					<?php endif; ?>
				</ul>
				<div class="footer-social">
					<?php westminster_social_links(); ?>
				</div>
			</div>

			<div class="footer-branding widget">
				<img class="footer-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/WH-logo-March2018.png" alt="<?php echo get_bloginfo('name'); ?>" />
			</div>

		</div>

		<!--<div id="sponsors" class="flex-container">
			<div class="sponsor-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/province-of-british-columbia.svg" alt="Province of British Columbia" />
			</div>
			<div class="sponsor-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fraser-health.svg" alt="Fraser Health" />
			</div>
			<div class="sponsor-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/british-columbia-lottery-corporation.svg" alt="British Columbia Lottery Corporation" />
			</div>
			<div class="sponsor-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/canadian-addiction-counsellors-certification-federation.svg" alt="Canadian Addiction Counsellors Certification Federation" />
			</div>
		</div>-->

		<?php echo do_shortcode('[logo_carousel_slider]'); ?>

		<div class="site-info">
			<p><span class="copyright">&copy; <?php echo( date('Y') )?> <?php echo get_bloginfo('name'); ?> &ndash; <?php echo get_bloginfo('description'); ?>.</span><br /> 
			<span class="webdev"><?php printf( __( 'Site by %2$s', 'westminster' ), 'westminster', '<a href="http://glaciermediadigital.ca" rel="designer" target="_blank">Glacier Digital</a>' ); ?></span></p>
		</div>

	</footer>
</div>

<?php wp_footer(); ?>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery('#primary-menu').slicknav({
		prependTo: '#masthead'
	});
});
</script>
<script>
jQuery('#header-search .fa-search').click(function() {
	jQuery('#sitesearchform').slideToggle();
});
jQuery('#header-search *').click(function (e) {
    e.stopPropagation();
});
</script>
<?php if ( is_page('photo-gallery') ): ?>
<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/lightbox.min.js"></script>
<?php endif; ?>
<script>
jQuery(document).ready(function(){jQuery("#faqs").accordion({heightStyle:"content",});});</script>
<script>jQuery(document).ready(function(){var o=400,t=500;jQuery(window).scroll(function(){jQuery(this).scrollTop()>o?jQuery(".back-to-top").fadeIn(t):jQuery(".back-to-top").fadeOut(t)}),jQuery(".back-to-top").click(function(o){return o.preventDefault(),jQuery("html, body").animate({scrollTop:0},t),!1})});</script>
<?php /* require_once($_SERVER['DOCUMENT_ROOT'] . "/westminsterhouse.php");*/ ?>

</body>
</html>
