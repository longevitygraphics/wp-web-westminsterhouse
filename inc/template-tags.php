<?php
/**
 * @package Westminster
 */

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Website Settings',
		'menu_title'	=> '',
		'menu_slug' 	=> 'website-settings',
		'capability'	=> 'edit_posts',
		'position' 		=> '3.2',
		'redirect'		=> false
	));
}

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyDDZH6TQBdLlRFqiMuPh-xfWFs2C69Dzz4');
}
add_action('acf/init', 'my_acf_init');

if ( ! function_exists( 'westminster_social_links' ) ) :
function westminster_social_links() { 
	$facebook 		= get_field('facebook', 'option');
	$twitter 		= get_field('twitter', 'option');
	$instagram 		= get_field('instagram', 'option');
	$pinterest 		= get_field('pinterest', 'option');
	$linkedin 		= get_field('linkedin', 'option');
	$googleplus 	= get_field('googleplus', 'option');
	$youtube 		= get_field('youtube', 'option');
	$vimeo 			= get_field('vimeo', 'option');
	$yelp 			= get_field('yelp', 'option');
	$social_links 	= (strlen($facebook) + strlen($twitter) + strlen($instagram) + strlen($pinterest) + strlen($linkedin) + strlen($googleplus) + strlen($youtube) + strlen($vimeo) + strlen($yelp) >0)?true:false;
	if ($social_links):
?>
	<ul class="social-links">
		<?php if( $facebook ): ?>
			<li><a href="<?php echo $facebook ?>" target="_blank"><i class="fa-fw fa fa-facebook"></i></a></li>
		<?php endif; ?>
		<?php if( $twitter ): ?>
			<li><a href="<?php echo $twitter ?>" target="_blank"><i class="fa-fw fa fa-twitter"></i></a></li>
		<?php endif; ?>
		<?php if( $instagram ): ?>
			<li><a href="<?php echo $instagram ?>" target="_blank"><i class="fa-fw fa fa-instagram"></i></a></li>
		<?php endif; ?>
		<?php if( $pinterest ): ?>
			<li><a href="<?php echo $pinterest ?>" target="_blank"><i class="fa-fw fa fa-pinterest"></i></a></li>
		<?php endif; ?>
		<?php if( $linkedin ): ?>
			<li><a href="<?php echo $linkedin ?>" target="_blank"><i class="fa-fw fa fa-linkedin"></i></a></li>
		<?php endif; ?>
		<?php if( $googleplus ): ?>
			<li><a href="<?php echo $googleplus ?>" target="_blank"><i class="fa-fw fa fa-google-plus"></i></a></li>
		<?php endif; ?>
		<?php if( $youtube ): ?>
			<li><a href="<?php echo $youtube ?>" target="_blank"><i class="fa-fw fa fa-youtube"></i></a></li>
		<?php endif; ?>
		<?php if( $vimeo ): ?>
			<li><a href="<?php echo $vimeo ?>" target="_blank"><i class="fa-fw fa fa-vimeo-square"></i></a></li>
		<?php endif; ?>
		<?php if( $yelp ): ?>
			<li><a href="<?php echo $yelp ?>" target="_blank"><i class="fa-fw fa fa-yelp"></i></a></li>
		<?php endif; ?>
	</ul>
	<?php endif; } endif;

if ( ! function_exists( 'westminster_numeric_navigation' ) ):
function westminster_numeric_navigation() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
			'show_all' => false,
			'prev_next' => true,
			'prev_text' => __('<i class="fa fa-angle-left"></i>'),
			'next_text' => __('<i class="fa fa-angle-right"></i>'),
        ));
    }
}
endif;

// Custom Staff search form.
if ( ! function_exists( 'westminster_staff_dept_search' ) ) :
function westminster_staff_dept_search() { ?>

	<form role="search" method="get" id="staffsearchform" action="<?php bloginfo('url'); ?>">
	<div>
		<?php function get_team_dept_dropdown($taxonomies, $args){
			$myterms = get_terms($taxonomies, $args);
			$optionname = "department";
			$emptyvalue = "";
			$output ="<select name='".$optionname."'><option value='".$emptyvalue."'>Select a Department</option>'";

			foreach($myterms as $term){
				$term_taxonomy=$term->taxonomy;
				$term_slug=$term->slug;
				$term_name =$term->name;
				$link = $term_slug;
				$output .="<option name='".$link."' value='".$link."'>".$term_name."</option>";
			}
			$output .="</select>";
			return $output;
		}

		$taxonomies = array('department');
		$args = array('order'=>'ASC','hide_empty'=>true);
		echo get_team_dept_dropdown($taxonomies, $args);
		?>
		<button type="submit" id="gosubmit" value="Go">Go</button>
	</div>
	</form>
	<?php
}
endif;

if ( ! function_exists( 'westminster_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function westminster_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'westminster' ),
		'' . $time_string . ''
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'westminster' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'westminster_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function westminster_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'westminster' ) );
		if ( $categories_list && westminster_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'westminster' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'westminster' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'westminster' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'westminster' ), esc_html__( '1 Comment', 'westminster' ), esc_html__( '% Comments', 'westminster' ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'westminster' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function westminster_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'westminster_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'westminster_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so westminster_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so westminster_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in westminster_categorized_blog.
 */
function westminster_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'westminster_categories' );
}
add_action( 'edit_category', 'westminster_category_transient_flusher' );
add_action( 'save_post',     'westminster_category_transient_flusher' );
