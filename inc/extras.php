<?php
/**
 * @package Westminster
 */

/**
 * @param array $classes Classes for the body element.
 * @return array
 */
function westminster_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'westminster_body_classes' );

// Custom Login Page
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/westminster-house.svg');
			background-size: 280px 93px;
            width: 280px;
            height: 93px;
            padding-bottom: 10px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return get_bloginfo('name');
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// Warn about changing Pages
add_action( 'current_screen', 'admin_page_warning' );
function admin_page_warning() {
    $current_screen = get_current_screen();
    if ( 'page' == $current_screen->post_type && 'edit' == $current_screen->base ) { ?>
    <div class="notice notice-warning">
        <p><?php _e( '<strong>CAUTION!</strong> Avoid changing Page URLs (slugs) as this can break your website. Please contact Glacier Media if you need changes made to page URLs.', 'westminster' ); ?></p>
    </div>
	<?php }
}
