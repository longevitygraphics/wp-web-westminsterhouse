<div class="team-member-list">
	<?php
		$args = array(
	        'showposts'	=> 4,
	        'post_type'		=> 'staff',

	    );

	    $result = new WP_Query( $args );

	    // Loop
	    if ( $result->have_posts() ) : ?>
			<div class="row">
				<div class="col-12"><h2 class="text-center color-primary">Meet our team</h2></div>
				<div class="col-12 text-right"><a class="color-secondary" href="/staff/"><u>View Full Team</u>  ></a></div>
	        <?php while( $result->have_posts() ) : $result->the_post(); 
	        	$name = get_the_title();
	        	$title = get_field('staff_title');
	        	$image = get_the_post_thumbnail_url();
	    ?>
	    	<div class="col-md-6 col-lg-3 mt-3 mb-3">
	    		<div class="single-team-member">
					<div class="image"><img src="<?php echo $image; ?>" alt="<?php echo $name; ?>"></div>
					<div class="description text-center">
						<div class="color-primary"><?php echo $name; ?></div>
						<div><?php echo $title; ?></div>
					</div>
	    		</div>
	    	</div>
			<?php
	        endwhile; ?>
	        </div>
	    <?php endif; // End Loop

	    wp_reset_query();
	?>
</div>