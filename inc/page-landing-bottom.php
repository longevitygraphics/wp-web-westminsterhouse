<?php
	$landing_section_enabled = get_field('landing_bottom_nav_enabled');
	$bottom_nav_mobile_text = get_field('bottom_nav_mobile_text');
?>

<?php if($landing_section_enabled == 1): ?>
	<div class="<?php if($bottom_nav_mobile_text){ echo 'd-none d-md-block '; } ?>landing-section-bottom my-5">
	<?php if( have_rows('bottom_nav_links') ): ?>
	    <?php while ( have_rows('bottom_nav_links') ) : the_row();
	        $title = get_sub_field('text')['title'];
	        $description = get_sub_field('text')['description'];
	        $link = get_sub_field('text')['button'];
	        $image = get_sub_field('image');
	        ?>
	    	<div>
	    		<div class="row no-gutters">

					<div class="title text-center col-md-6 bg-gray-light py-4">
						<div class="font-weight-bold h3 color-primary mb-1"><?php echo $title; ?></div>
						<div class="small"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" class="btn-primary d-inline-block"><?php echo $link['title']; ?></a>
					</div>
					<div class="image col-md-6" style="background-image: url(<?php echo $image['url']; ?>);"></div>

				</div>
	    	</div>
	    <?php endwhile; ?>
	<?php else :
	    // no rows found
	endif; ?>
	</div>
	<?php if ($bottom_nav_mobile_text): ?>
		<div class="d-md-none container">
			<?php echo $bottom_nav_mobile_text; ?>
		</div>
	<?php endif ?>
<?php endif; ?>

<script>
	// Windows Ready Handler

(function($) {

    $(document).ready(function(){
        $('.landing-section-bottom').slick({
        	arrows: true
        });
    });

}(jQuery));
</script>