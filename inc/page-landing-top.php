<?php
	$landing_section_enabled = get_field('landing_section_enabled');
	$links_per_row = get_field('top_nav_links_per_row');
?>

<?php if($landing_section_enabled == 1): ?>
	<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('top_nav_links') ): ?>
		<div class="d-flex grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('top_nav_links') ) : the_row();
	        $title = get_sub_field('text')['title'];
	        $description = get_sub_field('text')['description'];
	        $link = get_sub_field('text')['link'];
	        $image = get_sub_field('image');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
<?php endif; ?>

<!-- Global top banner starts here -->

<?php
	$top_nav_selector = get_field('top_nav_selector');
?>

<!-- about top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('about_links_per_row','option');
		$mobile_text = get_field('mobile_text'); 
	?>
	<?php if ($top_nav_selector == 'about'): ?>
		<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('about_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('about_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
		
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>

<!-- end of about top banner -->

<!-- treatment top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('treatment_programs_links_per_row','option');
		$mobile_text = get_field('mobile_text'); 
	?>
	<?php if ($top_nav_selector == 'treatment'): ?>
		<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('treatment_programs_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('treatment_programs_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>

<!-- end of treatment top banner -->

<!-- admissions top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('admissions_links_per_row','option');
		$mobile_text = get_field('mobile_text'); 
	?>
	<?php if ($top_nav_selector == 'admissions'): ?>
			<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('admissions_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('admissions_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>

<!-- end of admissions top banner -->

<!-- initiatives top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('initiatives_links_per_row','option');
		$mobile_text = get_field('mobile_text'); 
	?>
	<?php if ($top_nav_selector == 'initiatives'): ?>
			<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('initiatives_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('initiatives_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>

<!-- end of initiatives top banner -->

<!-- helps top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('helps_links_per_row','option'); 
		$mobile_text = get_field('mobile_text');
	?>
	<?php if ($top_nav_selector == 'help'): ?>
			<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('helps_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('helps_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>

<!-- end of initiatives top banner -->
<!-- media top banner -->
<?php if ($top_nav_selector): ?>
	<?php 
		$links_per_row = get_field('media_links_per_row','option');
		$mobile_text = get_field('mobile_text'); 
	?>
	<?php if ($top_nav_selector == 'media'): ?>
			<div class="container-fluid landing-section py-5 px-4 px-lg-5">
		<h1 class="text-secondary h2 mb-4 font-weight-bold"><?php the_title(); ?></h1>
	<?php if( have_rows('media_links', 'option') ): ?>
		<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
	    <?php while ( have_rows('media_links','option') ) : the_row();
	        $title = get_sub_field('text', 'option')['title'];
	        $description = get_sub_field('text', 'option')['description'];
	        $link = get_sub_field('text', 'option')['link'];
	        $link_target = $link['target'] ? $link['target'] : '_self';
	        $image = get_sub_field('image', 'option');
	        ?>
	    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

				<div class="image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<div class="overlay">
						<div class="text-center mb-2"><?php echo $description; ?></div>
						<a href="<?php echo $link['url']; ?>" target="<?php echo esc_attr($link_target); ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
					</div>
				</div>
				<div class="title text-center py-3">
					<div class="font-weight-bold"><?php echo $title; ?></div>
				</div>

			</div>
	    <?php endwhile; ?>
		</div>
		<?php if ($mobile_text): ?>
			<div class="d-md-none">
				<?php echo $mobile_text; ?>
			</div>
		<?php endif ?>
	<?php else :
	    // no rows found
	endif; ?>

	<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
	</div>
	<?php endif ?>
<?php endif ?>
<!-- end of media top banner -->