<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay">
			<div class="page-title">
				<?php $parent_link = get_permalink($post->post_parent); ?>
				<h2 class="title-lead"><a href="<?php echo $parent_link ?>"><?php echo get_the_title($post->post_parent); ?></a></h2>
				<h6><?php the_title(); ?></h6>
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); 
				get_template_part( 'template-parts/content', 'page' ); 
			endwhile; ?>

			<form method="GET" id="paymentform" class="payment-form" action="https://www.beanstream.com/scripts/payment/payment.asp">
				<fieldset>
					<legend>Step 1<br />
					<span class="required"><sup>*</sup> Required Fields</span></legend>
					<input type="hidden" name="merchant_id" value="300202433" title="Merchant ID" />
					<div class="payment-number">
						<label for="trnOrderNumber">Invoice No. </label>
						<input type="text" name="trnOrderNumber" />
					</div>
					<div class="payment-amount">
						<label for="trnAmount">Amount<span class="required"><sup>*</sup></span> <span class="currency">CAD$</span></label>
						<input type="text" name="trnAmount" />
					</div>
					<div class="payment-application">
						<label>Payment For </label>
						<select name="ref1">
							<option value="Invoice">Invoice</option>
							<option value="Medication">Medication</option>
							<option value="Allowance">Allowance</option>
							<option value="Merchandise">Merchandise</option>
							<option value="Donation">Donation</option>
							<option value="Raffle">Raffle</option>
						</select>
					</div>
					<div class="payment-button clear">
						<button type="submit" class="payment-submit" value="Submit Payment">Continue <i class="fa fa-angle-right"></i></button>
						<span class="secure">
							<a style="text-decoration: none" href="https://www.beanstream.com/scripts/valid_merchant.asp?merchantId=300202433" target="_new"><img src="https://www.beanstream.com/public/assets/images/media/beanstream_secure/beanstream_secure_light.gif" border="0" width="57" height="30" alt="Secure Electronic Payment Processing"></a>
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/visa.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/mastercard.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/amex.svg" alt="" />
							<img class="payment-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/discover.svg" alt="" />
						</span>
					</div>
				</fieldset>
			</form>

		</main>

	</div>

<?php get_footer(); ?>
