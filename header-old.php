<?php
/**
 * @package Westminster
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5JWHZL3');</script>
<!-- End Google Tag Manager -->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" />
<script src="https://use.typekit.net/kaw8nqr.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<meta name="google-site-verification" content="1Sd1zKIoVL2gPCc_hIixVGd8Mb-MTOyT3jcKITn5jNI" />
<script async src='https://tag.simpli.fi/sifitag/46a58a90-dc3b-0134-0bc4-0cc47a63c1a4'></script>
<?php include_once("inc/analyticstracking.php"); ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JWHZL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php if(!is_front_page()): ?>
<!-- Google Code for Test Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 826197508;
var google_conversion_label = "CA10CMCMpYABEISM-4kD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/826197508/?label=CA10CMCMpYABEISM-4kD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php endif; ?>

<?php require_once("inc/Mobile_Detect.php"); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'westminster' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<nav id="site-navigation" class="main-navigation clear" role="navigation">
			<div id="header-search">
				<i class="fa fa-search"></i>
				<?php get_search_form(); ?>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav>

		<?php 
			$phone1	= get_field('contact_phone1', 'option'); 
			$phone2	= get_field('contact_phone2', 'option'); 
			$email 	= get_field('contact_email', 'option'); 					
		?>

		<?php $detect = new Mobile_Detect; if ( ! $detect->isMobile() ): ?>
		<div class="header-contact-cta">
			<?php if( $phone1 ): ?>
			<p>Call us Toll Free at <a class="phone-no" href="tel:<?php echo $phone1 ?>"><?php echo $phone1 ?></a> <span class="highlight">24 hours</span> a day, <span class="highlight">7 Days</span> a week</p>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<div class="site-branding clear">
			<div class="header-logo">
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img class="site-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/WH-logo-March2018.png" alt="<?php echo get_bloginfo('name'); ?> <?php echo get_bloginfo('description'); ?>" /></a></p>
			</div>
			<div class="header-social">
				<?php westminster_social_links(); ?>
			</div>
			<div class="header-contact">
				<?php if( $phone1 ): ?>
				<p>Call us Toll Free at <a class="phone-no" href="tel:<?php echo $phone1 ?>"><?php echo $phone1 ?></a><br /><span class="highlight">24 hours</span> a day, <span class="highlight">7 Days</span> a week</p>
				<?php endif; ?>
			</div>
		</div>

	</header>

	<div id="content" class="site-content">
