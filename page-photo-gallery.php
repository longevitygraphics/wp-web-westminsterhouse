<?php
/**
 * Template Name: Photo Gallery Page
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<?php $parent_link = get_permalink($post->post_parent); 
				if ($parent_link): ?>
			<h2 class="title-lead"><a href="<?php echo $parent_link ?>"><?php echo get_the_title($post->post_parent); ?></a></h2>
			<?php endif; ?>
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ): 
			while ( have_posts() ): the_post(); ?>
			
			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<h2>Photo Gallery</h2>
			<aside id="photo-gallery" class="flex-container">
				<?php $images = get_field('photo_gallery');
					if( $images ): ?>
				<?php foreach( $images as $image ): ?>
					<a class="fancybox" href="<?php echo $image['sizes']['large']; ?>" data-lightbox="profile">
						<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				<?php endforeach; ?>
				<?php endif; ?>
			</aside>
			</div>

			<?php endwhile; ?>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
