<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<!-- CTA -->
	<?php
	if( have_rows('cta') ): ?>
		<div class="home-cta landing-section">
	    <?php while ( have_rows('cta') ) : the_row();
	        $title = get_sub_field('title');
	        $image = get_sub_field('image');
	        $description = get_sub_field('description');
	        $link = get_sub_field('link');
	        ?>

	        <div>
	        	<div class="panel">
	        		<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
	        		<div class="overlay">
	        			<div class="description">
	        				<?php echo $description; ?>
	        			</div>
	        			<?php if($link): ?>
						<a href="<?php echo $link['url']; ?>">Learn More ></a>
	        			<?php endif; ?>
	        		</div>
	        	</div>
	        	<div class="title"><?php echo $title; ?></div>
	        </div>
	    <?php endwhile; ?>
	    </div>
	<?php else :
	    // no rows found
	endif;
	?>
	<!-- end CTA -->

	<!-- Content -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ): 
				while ( have_posts() ): the_post(); ?>
			<div class="entry-content">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/acorn-favicon.png" alt="" class="favicon">
				<?php the_content(); ?>
			</div>
			<?php endwhile; endif; ?>
		</main>
	</div>
	<!-- end Content -->

	<div id="intro_section">
		<?php
			$intro_section_text = get_field('intro_section_text');
			$intro_button_text = get_field('intro_button_text');
			$intro_button_link = get_field('intro_button_link');
			$intro_button_colour = get_field('intro_button_colour');
		?>
		<div class="intro-title">
			<h6><?php echo $intro_section_text ?></h6>
			<?php if( get_field('intro_show_button') ): ?>
			<a class="btn <?php echo $intro_button_colour['value']; ?> more" href="<?php echo $intro_button_link ?>"><?php echo $intro_button_text ?></a>
			<?php endif; ?>
		</div>
	</div>

	<?php $video = get_field('homepage_video');
		if( $video ): ?>
	<div class="home-video">
		<div class="embed-container">
		<?php echo $video ?>
		</div>
		<a class="btn transparent more" href="<?php echo esc_url( home_url( '/news-and-events/videos/' ) ); ?>"><i class="fa fa-youtube-play"></i> More Videos</a>
	</div>
	<?php endif; ?>

	<div id="home-teasers" class="flex-container">
		<div class="widget">
			<a href="<?php echo esc_url( home_url( '/about/our-mission/' ) ); ?>"><i class="fa fa-leaf"></i></a>
			<h6><a href="<?php echo esc_url( home_url( '/about/our-mission/' ) ); ?>">Our Mission</a></h6>
		</div>
		<div class="widget">
			<a href="<?php echo esc_url( home_url( '/photo-gallery/' ) ); ?>"><i class="fa fa-camera"></i></a>
			<h6><a href="<?php echo esc_url( home_url( '/photo-gallery/' ) ); ?>">Gallery</a></h6>
		</div>
		<div class="widget">
			<a href="<?php echo esc_url( home_url( '/testimonials/' ) ); ?>"><i class="fa fa-quote-left"></i></a>
			<h6><a href="<?php echo esc_url( home_url( '/testimonials/' ) ); ?>">Testimonials</a></h6>
		</div>
		<div class="widget">
			<a href="<?php echo esc_url( home_url( '/admissions/referral-package/' ) ); ?>"><i class="fa fa-heart"></i></a>
			<h6><a href="<?php echo esc_url( home_url( '/admissions/referral-package/' ) ); ?>">Referral Package</a></h6>
		</div>
	</div>

	<div id="latest_news">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/acorn-favicon.png" alt="" class="favicon">
		<h6>Latest News</h6>
		<div class="inner-wrapper flex-container">
		<?php $query = new WP_Query( 
			array( 
				'posts_per_page' => 3,
				'cat' 			=> '-12',
			) 
		);
		if ( $query->have_posts() ): while ( $query->have_posts() ): $query->the_post(); { 
			get_template_part( 'template-parts/content', 'archives' ); 
		} endwhile; endif; wp_reset_postdata(); ?>
		</div>
	</div>

<?php get_footer(); ?>
