<?php
/**
 * @package Westminster
 */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

if ( ! function_exists( 'westminster_setup' ) ) :

function westminster_setup() {
	load_theme_textdomain( 'westminster', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'utility' => esc_html__( 'Utility Menu', 'westminster' ),
		'primary' => esc_html__( 'Primary Menu', 'westminster' ),
		'mobile' => esc_html__( 'Mobile Menu', 'westminster' ),
		'footer' => esc_html__( 'Footer Menu', 'westminster' ),
	) );
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );
	add_theme_support( 'custom-background', apply_filters( 'westminster_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // westminster_setup
add_action( 'after_setup_theme', 'westminster_setup' );

/**
 * @global int $content_width
 */
function westminster_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'westminster_content_width', 640 );
}
add_action( 'after_setup_theme', 'westminster_content_width', 0 );

function westminster_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'westminster' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'westminster_widgets_init' );

// Enqueue scripts
function westminster_scripts() {
	wp_enqueue_style( 'westminster-style', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ) );
	wp_enqueue_style( 'longevity-style', get_stylesheet_directory_uri() . '/css/longevity.css', array(), filemtime( get_stylesheet_directory() . '/css/longevity.css' ) );
	wp_enqueue_style( 'slicknav-style', get_stylesheet_directory_uri() . '/css/slicknav.css', array(), filemtime( get_stylesheet_directory() . '/css/slicknav.css' ) );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' ); 
	wp_enqueue_style( 'lightbox-style', get_stylesheet_directory_uri() . '/css/lightbox.min.css', array(), filemtime( get_stylesheet_directory() . '/css/lightbox.min.css' ) );

	wp_enqueue_style( 'slick-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array(), false );
	wp_enqueue_style( 'slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', array(), false );
	wp_enqueue_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), '20151121', true );

    wp_enqueue_script( 'westminster-navigation', get_template_directory_uri() . '/js/jquery.slicknav.min.js', array('jquery'), '20151121', true );
	wp_enqueue_script( 'westminster-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array('jquery'), '20191205', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'westminster_scripts' );

function my_acf_google_map_api( $api ){
	// $api['key'] = 'AIzaSyB26OwHJm3ihrHnjvmC-fZebBzmMBd9nBg'; 
	$api['key'] = 'AIzaSyDMP_EAB7zmhPNrWxc6_W31SM-FB1l_VRI';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// Load Dependencies
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/jetpack.php';


function create_taxonomy(){

	register_taxonomy(
		'page-category',
		'page',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'page-category', "with_front" => false ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );


function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		array(
	        'title' => 'Text Colors',
	        'items' =>  array(
    			array(  
					'title' => 'Pink',  
					'inline' => 'span',  
					'classes' => 'text-primary',
					'wrapper' => true
				),
				array(  
					'title' => 'Gray',  
					'inline' => 'span',  
					'classes' => 'text-secondary',
					'wrapper' => true
				)
	        )
	    ),
	    array(
	        'title' => 'Buttons',
	        'items' =>  array(
    			array(  
					'title' => 'Pink',  
					'selector' => 'a',  
					'classes' => 'btn btn-primary'
				),
				array(  
					'title' => 'Gray',  
					'selector' => 'a',  
					'classes' => 'btn btn-secondary'
				)
	        )
	    )
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  


add_filter('acf/fields/flexible_content/layout_title', 'lg_flexible_content_layout_title', 10, 4);

function lg_flexible_content_layout_title( $title, $field, $layout, $i ) {
  // remove layout title from text
   $title = '';
   $new_title = get_sub_field('block_title');
   
   if($new_title){
     return $new_title;
   }else{
     return $title;
   }
}