<?php
/**
 * @package Westminster
 */

get_header(); ?>
	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<!-- <?php global $post; 
				$blog_page 	= get_option('page_for_posts');
				$blog_title	= get_the_title( $blog_page ); 
				$blog_url 	= get_permalink( $blog_page ); 
			?>
			<h2 class="title-lead"><a href="<?php echo $blog_url; ?>"><?php echo $blog_title; ?></a></h2> -->
			<h2 class="title-lead"><a href="/how-to-help">About</a></h2>
			<h6>Testimonials</h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<div id="archives" class="flex-container">
			<?php while ( have_posts() ): the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('archive'); ?>>
					<header class="entry-header">
						<div class="entry-thumbnail">
							<a href="<?php the_permalink(); ?>">
								<?php if( has_post_thumbnail() ): the_post_thumbnail('thumbnail'); 
									else: echo '<img src="'. get_stylesheet_directory_uri() .'/images/testimonial-default-thumb.jpg" alt="" />'; 
								endif; ?></a>
						</div>
						<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<a href="<?php the_permalink(); ?>">Read Testimonial</a>
					</header>
				</article>

			<?php endwhile; ?>
			</div>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
