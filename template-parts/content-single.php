<?php
/**
 * @package Westminster
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php westminster_posted_on(); ?>
		</div>
	</header>

	<?php if ( has_post_thumbnail() ): ?>
	<div class="entry-thumbnail">
		
	</div>
	<?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'westminster' ),
				'after'  => '</div>',
			) );
		?>
	</div>

	<footer class="entry-footer">
		<?php westminster_entry_footer(); ?>
	</footer>
</article>
