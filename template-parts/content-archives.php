<?php
/**
 * @package Westminster
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('archive'); ?>>
	<div class="thumb-wrapper">
		<div class="entry-thumbnail">
			<a href="<?php the_permalink(); ?>">
			<?php if ( has_post_thumbnail() ):
				the_post_thumbnail('medium'); 
			else: 
				echo '<img src="'. get_stylesheet_directory_uri() .'/images/default-thumbnail.png" alt="" />'; 
			endif; ?>
			</a>
		</div>
	</div>
	<?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
	<p class="read-more"><a class="more" href="<?php the_permalink(); ?>">Read Story</a><?php the_date('M j', '<span>', '</span>'); ?></p>
</article>
