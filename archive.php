<?php
/**
 * @package Westminster
 */

get_header(); ?>
	
	<?php if(is_category()) : ?>
	<!-- media top banner -->
		<!-- <?php 
			$links_per_row = get_field('media_links_per_row','option');  
			$term = get_queried_object();
			$mobile_text = get_field('mobile_text', $term);
		?>
				<div class="container-fluid landing-section py-5 px-4 px-lg-5">
			<h1 class="text-capitalize text-secondary h2 mb-4 font-weight-bold"><?php echo $term->slug ?></h1>
		<?php if( have_rows('media_links', 'option') ): ?>
			<div class="<?php if($mobile_text){ echo 'd-none d-md-flex'; }else{echo 'd-flex';} ?> grid-list-hover-overlay  row align-items-start justify-content-start" style="margin-top: -7.5px;margin-bottom: -7.5px;">
		    <?php while ( have_rows('media_links','option') ) : the_row();
		        $title = get_sub_field('text', 'option')['title'];
		        $description = get_sub_field('text', 'option')['description'];
		        $link = get_sub_field('text', 'option')['link'];
		        $image = get_sub_field('image', 'option');
		        ?>
		    	<div style="padding-top: 7.5px; padding-bottom: 7.5px;" class="col-12 col-sm-6 col-md-4 col-lg-<?php echo $links_per_row; ?> d-block">

					<div class="image">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<div class="overlay">
							<div class="text-center mb-2"><?php echo $description; ?></div>
							<a href="<?php echo $link['url']; ?>" class="btn-landing-page"><?php echo $link['title']; ?> ></a>
						</div>
					</div>
					<div class="title text-center py-3">
						<div class="font-weight-bold"><?php echo $title; ?></div>
					</div>

				</div>
		    <?php endwhile; ?>
			</div>
			<?php if ($mobile_text): ?>
				<div class="d-md-none">
					<?php echo $mobile_text; ?>
				</div>
			<?php endif ?>
		<?php else : ?>
		    
		<div id="page-thumb">
			<div class="overlay"></div>
			<div class="page-title">
				<?php global $post; 
					$blog_page 	= get_option('page_for_posts');
					$blog_title	= get_the_title( $blog_page ); 
					$blog_url 	= get_permalink( $blog_page ); 
				?>
				<h2 class="title-lead"><a href="<?php echo $blog_url; ?>"><?php echo $blog_title; ?></a></h2>
				<h6>Events</h6>
			</div>
		</div>
		<?php endif; ?>

		<img class="site-icon" src="/wp-content/themes/wp-web-westminsterhouse/images/acorn-favicon.png" alt="">
		</div> -->
	<!-- end of media top banner -->
	<?php endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ): ?>

			<div id="archives" class="flex-container">
			<?php while ( have_posts() ) : the_post(); 
				get_template_part( 'template-parts/content', 'archives' ); 
			endwhile; ?>
			</div>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
