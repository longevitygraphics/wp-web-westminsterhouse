<?php
/**
 * @package Westminster
 */

get_header(); ?>
	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h6><?php the_title(); ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); 
				get_template_part( 'template-parts/content', get_post_format() ); 
			endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
