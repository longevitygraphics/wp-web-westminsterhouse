<?php
/**
 * Template Name: Programs Archive Page
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h6>Treatment Programs</h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ): 
			while ( have_posts() ): the_post(); ?>
			
			<div class="entry-content">
				<?php the_content(); ?>
			</div>

			<h2>Our Programs</h2>
			<div id="archives" class="flex-container">
			<?php $query = new WP_Query( array( 'post_type' => 'program', 'posts_per_page' => 12 ) );
				if ( $query->have_posts() ): while ( $query->have_posts() ): $query->the_post(); { ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class('archive'); ?>>
					<header class="entry-header">
						<div class="entry-thumbnail">
							<a href="<?php the_permalink(); ?>">
								<?php if( has_post_thumbnail() ): the_post_thumbnail('medium'); 
									else: echo '<img src="'. get_stylesheet_directory_uri() .'/images/default-thumbnail.png" alt="" />'; 
								endif; ?></a>
						</div>
						<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</header>
				</article>
			<?php } endwhile; endif; wp_reset_postdata(); ?>
			</div>

			<?php endwhile; ?>

			<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="pagination">
				<?php westminster_numeric_navigation(); ?>
			</div>
			<?php endif; ?>

		<?php else: 
			get_template_part( 'template-parts/content', 'none' ); 
		endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
