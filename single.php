<?php
/**
 * @package Westminster
 */

get_header(); ?>

	<div id="page-thumb">
		<div class="overlay"></div>
		<div class="page-title">
			<h2 class="title-lead"><a href="<?php echo esc_url( home_url('/news-and-events/') ); ?>">News &amp Events</a></h2>
			<h6><?php $categories = get_the_category();
				if ( ! empty( $categories ) ) {
				echo esc_html( $categories[0]->name );   
			} ?></h6>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); 
			get_template_part( 'template-parts/content', 'single' ); 
			the_post_navigation(); 
		endwhile; ?>

		</main>
	</div>

<?php get_footer(); ?>
